/*
 * Test de la classe String
 *
 * Autor : Mathieu Monteverde et Michela Zucca
 * 
 * Date  : 22.03.2017
 * File  : main.cpp
 */
#include <cstdlib>

#include "cstring.h" 
#include <iostream>
#include <stdio.h>
#include <stdexcept>
#include <string.h>

using namespace std;
/**
 * Fonctions des tests
 */
void testSize(const String& s, int expected);
void testAt(const String& s, unsigned index, char expected);
void testAtRef(const String& s, unsigned int index, char replace);
void testGetString(const String& s, const char* expected);
void testEquals(const String& s1, const String& s2, bool expected);
void testReplace(String& s1, const String& s2);
void testAppend(const String& s1, const String& s2);
void testSubString(const String & s1, int first, int last);

void testOpAffectation(String& s1, const String& s2); // opérateur = 
void testOpEgal(const String& s1, const String& s2, bool expected); // opérateur ==
void testOpNotEgal(const String& s1, const String& s2, bool expected); // opérateur !=
void testOpAddition(const String& s1, const String& s2); // opérateur +
void testOpAffectationAddition(String& s1, const String& s2); // opérateur +=
void testOpCrochet(const String& s1, int index, char expected); // opérateur[]
void testOpCrochetRef(String& s1, int index, char replace);

int main(int argc, char** argv) {

   String c1;
   String c2('c');
   String c3("Chaine");
   String c4(1);
   String c5(2.5);
   String c6(true);
   String c3Cpy(c3);

   cout << "-- TESTS DE LA CLASSE STRING --  " << endl;
   cout << "Test constructeur : String c1 --> <" << c1 << ">" << endl;
   cout << "Test constructeur : String c2(c) --> <" << c2 << ">" << endl;
   cout << "Test constructeur : String c3(chaine) --> <" << c3 << ">" << endl;
   cout << "Test constructeur : String c4(1) --> <" << c4 << ">" << endl;
   cout << "Test constructeur : String c5(2.5)  --> <" << c5 << ">" << endl; 
   cout << "Test constructeur : String c6(true) --> <" << c6 << ">" << endl;
   cout << "Test constructeur : String c3Cpy(c3) --> <" << c3Cpy << ">" << endl;

   // Test size()
   cout << endl;
   testSize(c1, 0);
   testSize(c2, 1);
   testSize(c3, 6);

   // Test at()
   cout << endl;
   testAt(c3, 2, 'a');
   testAt(c1, 5, '-'); // out of range
   String test("Chaine");
   testAtRef(test, 2, 'B');
   testAtRef(test, 5, 'B');

   // Test getString()
   cout << endl;
   testGetString(c3, "Chaine");
   testGetString(c2, "c");
   testGetString(c1, "");

   // Test equal()
   cout << endl;
   testEquals(c2, c3, false);
   testEquals(c3, c3Cpy, true);

   // Test replace()
   cout << endl;
   String test1("abc");
   String test2("xyz");
   testReplace(test1, test2);

   // Test append()
   cout << endl;
   test1 = "Salut";
   test2 = " vous";
   testAppend(test1, test2);
   test1 = "123";
   testAppend(test1, test1);

   // Test substring()
   cout << endl;
   test1 = "Salut a tous";
   testSubString(test1, 2, 8);
   testSubString(test1, 8, 2);
   testSubString(test1, 1, 20);
   testSubString(c1, 0, 0);
   testSubString(test1, 0, 0);

   // Test opreator =
   cout << endl;
   test1 = "Salut";
   test2 = "Aurevoir";
   testOpAffectation(test1, test2);

   // Test operator ==
   cout << endl;
   test1 = "Salut";
   test2 = "Tout le monde";
   testOpEgal(test1, test2, false);
   test1 = "Salut";
   test2 = "Salut";
   testOpEgal(test1, test2, true);

   // Test operator != 
   cout << endl;
   test1 = "Salut";
   test2 = "Tout le monde";
   testOpNotEgal(test1, test2, true);
   test1 = "Salut";
   test2 = "Salut";
   testOpNotEgal(test1, test2, false);

   // Test operator +
   cout << endl;
   test1 = "debut";
   test2 = "fin";
   testOpAddition(test1, test2);
   test2 = "";
   testOpAddition(test1, test2);

   // Test operator +=
   cout << endl;
   test1 = "Salut";
   test2 = " vous";
   testOpAffectationAddition(test1, test2);
   test1 = "123";
   testOpAffectationAddition(test1, test1);

   // Test operator []
   cout << endl;
   test1 = "Salut";
   testOpCrochet(test1, 2, 'l');
   testOpCrochet(test1, 20, '-'); // out of range
   testOpCrochetRef(test1, 2, 'b');
   testOpCrochetRef(test1, 20, 'b'); // out of range

   return 0;
}

void testSize(const String & s, int expected) {
   cout << "Test s.size() [expected,result] --> s <" << s << "> ";
   cout << "[" << expected << ",";

   int result = (int)s.size();
   cout << result << "] ";
   cout << (result == expected ? " passed " : " failed") << endl;
}

void testAtRef(const String& s, unsigned int index, char replace) {
   cout << "Test char& = s.at(" << index << ") remplacé par <"
           << replace << "> [expected, result] --> s: <" << s << "> ";
   bool passed = true;
   // Copie la chaine
   char* cpy = new char[s.size() + 1];
   strcpy(cpy, s.getString());

   if (index > s.size() || s.size() == 0) {
      try {
         cout << "[out of range, ";
         char& c = ((String&) s).at(index);
         passed = false;
         cout << ", " << c << "]";
      } catch (out_of_range e) {
         passed = true;
         cout << e.what() << "]";
      }
   } else {
      cpy[index] = replace;
      char& c = ((String&) s).at(index);
      c = replace;
      strcmp(cpy, s.getString()) ? passed = false : passed = true;
      cout << "[" << cpy << ", " << s << "] ";
   }
   cout << (passed ? " passed" : " failed") << endl;
}

void testAt(const String& s, unsigned int index, char expected) {
   cout << "Test  char = s.at(" << index
           << ") [expected, result] --> s: <" << s << "> ";
   char result;
   bool passed = true;
   // Doit provoquer une exception
   if (index > s.size() || s.size() == 0) {
      try {
         result = s.at(index);
         cout << " [" << expected << "," << result << "]";
         passed = false;
      } catch (out_of_range e) {
         passed = true;
         cout << "[out of range, " << e.what() << "]";
      }
   } else {
      result = s.at(index);
      cout << "[" << expected << ", " << result << "] ";
      result == expected ? passed = true : passed = false;
   }
   cout << (passed ? " passed" : " failed") << endl;
}

/**
 * Test la fonction getString()
 * @param s
 * @param expected
 */
void testGetString(const String& s, const char* expected) {
   cout << "Test s.getString() [expected, result]  --> s: <" << s << "> [";

   const char* result = s.getString();
   bool passed = !strcmp(result, expected);

   cout << expected << ", " << result << "]" 
           << (passed ? " passed" : " failed") << endl;
}

void testEquals(const String& s1, const String& s2, bool expected) {
   cout << "Test s1.equals(const String& s2) [expected, result] --> s1: <" 
           << s1 << "> s2:<" << s2 << "> [" 
           << (expected ? "true, " : "false, ");
   bool passed = s1.equals(s2);
   cout << (passed ? "true, " : "false, ") << "] "
           << (passed == expected ? " passed" : " failled") << endl;

   cout << "Test s1.equals(const char* s2)   [expected, result] --> s1: <"
           << s1 << "> s2: <" << s2 << "> [" 
           << (expected ? "true, " : "false, ");
   passed = s1.equals(s2.getString());
   cout << (passed ? "true, " : "false, ") << "] "
           << (passed == expected ? " passed" : " failled") << endl;
}

void testReplace(String& s1, const String& s2) {
   cout << "Test s1.replace(const String& s2) [expected, result] --> s1: <"
           << s1 << "> s2:<" << s2 << "> [" << s2 << ", ";
   bool passed;
   s1.replace(s2);
   passed = s1.equals(s2);
   cout << s1 << "]" << (passed ? " passed" : " failed") << endl;

   char test[5] = "test";
   cout << "Test s1.replace(cont char* c)     [expected, result] --> s1: <"
           << s1 << "> s2:<" << test << "> [" << test << ",";
   s1.replace(test);
   passed = s1.equals(test);
   cout << s1 << "]" << (passed ? " passed" : " failed") << endl;
}

void testAppend(const String &s1, const String& s2) {
   cout << "Test s1.append(const String& s2)  [expected, result] --> s1: <"
           << s1 << "> s2: <" << s2 << "> [" << s1 << s2 << ", ";

   bool passed;
   char temp[s1.size() + s2.size() + 1];
   temp[s1.size() + s2.size()] = '\0';
   strncpy(temp, s1.getString(), s1.size());
   strncpy(temp + s1.size(), s2.getString(), s2.size());

   String copy(s1);
   copy.append(s2);
   passed = copy.equals(temp);
   cout << copy << "] " << (passed ? " passed" : " failled") << endl;

   cout << "Test s1.append(const char* s2)    [expected, result] --> s1: <"
           << s1 << "> s2: <" << s2 << "> [" << s1 << s2 << ", ";
   copy.replace(s1);
   copy.append(s2.getString());
   passed = copy.equals(temp);
   cout << copy << "] " << (passed ? " passed" : " failled") << endl;
}

void testSubString(const String& s, int first, int last) {
   cout << "Test s1.subString(" << first << "," << last
           << ") [expected, result] --> s1: <" << s << "> [";
   bool passed;
   String result;
   // Condition du test vallant une erreur
   if (first > last || last > (int)s.size()) {
      try {
         result = s.substring(first, last);
         passed = false;
      } catch (range_error e) {
         passed = true;
         cout << "range error, " << e.what() << "]";
      } catch (out_of_range e) {
         passed = true;
         cout << "out of range, " << e.what() << "]";
      }
   } else {
      int size = last - first + 1;
      char* temp = new char[size + 1];
      temp[size] = '\0';
      strncpy(temp, ((s.getString()) + first), size);

      result = s.substring(first, last);
      passed = result.equals(temp);
      cout << temp << ", " << result << "] ";
      delete[] temp;
   }
   cout << (passed ? " passed" : " failed") << endl;
}

void testOpAffectation(String& s1, const String& s2) {
   cout << "Test s1 = s2 [expected, result] --> s1: <" << s1
           << "> s2: <" << s2 << ">  --> [" << s2 << ", ";

   bool passed, noModif;
   s1 = s2;
   passed = s1.equals(s2);
   cout << s1 << "] ";
   // Vérif que la modification de s1 n'entraine pas la modification de s2
   s1.append("lala");
   noModif = !s1.equals(s2);
   cout << (passed && noModif ? " passed" : " failed") << endl;
}

void testOpEgal(const String& s1, const String& s2, bool expected) {
   cout << "Test String == String [expected, result] --> s1: <" << s1
           << "> s2: <" << s2 << "> [" << (expected ? "true, " : "false, ");
   bool passed;
   passed = (s1 == s2);
   cout << (passed ? "true" : "false") << "]"
           << (passed == expected ? " passed" : " failled") << endl;

   cout << "Test  char* == String [expected, result] --> s1: <" << s1
           << "> s2: <" << s2 << "> [" << (expected ? "true, " : "false, ");
   passed = (s1.getString() == s2);
   if (passed)
      passed = (s1 == s2.getString());
   cout << (passed ? "true" : "false") << "]"
           << (passed == expected ? " passed" : " failled") << endl;
}

void testOpNotEgal(const String& s1, const String& s2, bool expected) {
   cout << "Test String != String [expected, result] --> s1: <" << s1
           << "> s2: <" << s2 << "> [" << (expected ? "true, " : "false, ");
   bool passed;
   passed = (s1 != s2);
   cout << (passed ? "true" : "false") << "]"
           << (passed == expected ? " passed" : " failled") << endl;

   cout << "Test  char* != String [expected, result] --> s1: <" << s1
           << "> s2: <" << s2 << "> [" << (expected ? "true, " : "false, ");
   passed = (s1.getString() != s2);
   if (passed)
      passed = (s1 != s2.getString());
   cout << (passed ? "true" : "false") << "]"
           << (passed == expected ? " passed" : " failled") << endl;
}

void testOpAffectationAddition(String& s1, const String& s2) {
   cout << "Test String += String [expected, result] --> s1: <" << s1
           << "> s2: <" << s2 << "> [" << s1 << s2 << ", ";
   bool passed;
   char temp[s1.size() + s2.size() + 1];
   temp[s1.size() + s2.size()] = '\0';
   strncpy(temp, s1.getString(), s1.size());
   strncpy(temp + s1.size(), s2.getString(), s2.size());
   s1 += s2;
   passed = s1.equals(temp);
   cout << s1 << "] " << (passed ? " passed" : " failed") << endl;
}

void testOpAddition(const String& s1, const String& s2) {
   cout << "Test String + String [expected, result] --> s1: <" << s1
           << "> s2: <" << s2 << "> [" << s1 << s2 << ", ";
   bool passed;
   char temp[s1.size() + s2.size() + 1];
   temp[s1.size() + s2.size()] = '\0';
   strncpy(temp, s1.getString(), s1.size());
   strncpy(temp + s1.size(), s2.getString(), s2.size());

   String result = s1 + s2;
   passed = result.equals(temp);
   // Vérification s1 et s2 non modifée sauf si l'une est chaine vide
   if (passed && s1 != "" && s2 != "")
      passed = (result != s1 && result != s2);
   cout << result << "] " << (passed ? " passed" : " failed") << endl;

   cout << "Test  char* + String [expected, result] --> s1: <" << s1
           << "> s2: <" << s2 << "> [" << s1 << s2 << ", ";
   result = s1.getString() + s2;
   passed = result.equals(temp);
   // Test intervertion des paramètres
   if (passed) {
      result = s1 + s2.getString();
      passed = result.equals(temp);
   }
   // Vérification s1 et s2 non modifée sauf si l'une est chaine vide
   if (passed && s1 != "" && s2 != "")
      passed = (result != s1 && result != s2);
   cout << result << "] " << (passed ? " passed" : " failed") << endl;
}

void testOpCrochet(const String& s, int index, char expected) {
   cout << "Test  char = s.at(" << index
           << ") [expected, result] --> s: <" << s << "> ";
   char result;
   bool passed = true;
   // Doit provoquer une exception
   if (index > (int)s.size() || (int)s.size() == 0) {
      try {
         result = s[index];
         cout << " [" << expected << "," << result << "]";
         passed = false;
      } catch (out_of_range e) {
         passed = true;
         cout << "[out of range, " << e.what() << "]";
      }
   } else {
      result = s[index];
      cout << "[" << expected << ", " << result << "] ";
      result == expected ? passed = true : passed = false;
   }
   cout << (passed ? " passed" : " failed") << endl;
}

void testOpCrochetRef(String& s, int index, char replace) {
    cout << "Test char& = s.at(" << index << ") remplacé par <"
            << replace << "> [expected, result] --> s: <" << s << "> ";
   bool passed = true;
   // Copie la chaine
   char* cpy = new char[s.size() + 1];
   strcpy(cpy, s.getString());

   if (index > (int)s.size() || (int)s.size() == 0) {
      try {
         cout << "[out of range, ";
         char& c = ((String&) s)[index];
         passed = false;
         cout << ", " << c << "]";
      } catch (out_of_range e) {
         passed = true;
         cout << e.what() << "]";
      }
   } else {
      cpy[index] = replace;
      char& c = ((String&) s)[index];
      c = replace;
      strcmp(cpy, s.getString()) ? passed = false : passed = true;
      cout << "[" << cpy << ", " << s << "] ";
   }
   cout << (passed ? " passed" : " failed") << endl;
}