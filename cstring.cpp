/*
 * Définition de la class String qui permet la manipulation de chaines de
 * caractères. 
 *
 * Author : Mathieu Monteverde et Michela Zucca
 * 
 * Date  : 21.03.2017
 * File  : cString.cpp
 */

#include "cstring.h"

#include <stddef.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <stdio.h>


using namespace std;


/**
 * Constructeur vide.
 */
String::String() {
   init(0);
}

/**
 * Constructeur à partir d'une chaine de caractères C.
 */
String::String(const char* s) {
   init(strlen(s));
   strcpy(string, s);
}

/**
 * Constructeur à partir d'un caractère.
 */
String::String(const char c) {
   init(1);
   string[0] = c;
}

/**
 * Constructeur à partir d'un entier.
 */
String::String(const int i) {
   // Buffer factice utilisé pour calculer la longueur nécessaire pour écrire i
   static char test[1] = {'\0'};
   // Calcule la longueur nécessaire pour représenter i en chaine de caractère
   size_t len = snprintf(test, 1, "%d", i);
   // initialiser la chaine
   init(len);
   // Ecrire i dans string
   snprintf(string, len + 1, "%d", i);
}

/**
 * Constructeur à partir d'un double.
 */
String::String(const double d) {
   // Buffer factice utilisé pour calculer la longueur nécessaire pour écrire d
   static char test[1] = {'\0'};
   // Calcule la longueur nécessaire pour représenter d en chaine de caractère
   size_t len = snprintf(test, 1, "%.*g", DBL_DIG, d);
   // initialiser la chaine
   init(len);
   // Ecrire i dans string
   snprintf(string, len + 1, "%.*g", DBL_DIG, d);
}

/**
 * Constructeur à partir d'un booléen.
 */
String::String(const bool b) : String(b ? "true" : "false") {
}

/**
 * Constructeur de recopie.
 */
String::String(const String& s) : String(s.string) {

}

/**
 * Initialisation générale de l'objet.
 */
void String::init(size_t len) {
   string = new char[len + 1];
   string[len] = '\0';
}

/**
 * Destructeur.
 */
String::~String() {
   delete[] string;
}



/**
 * Méthodes
 */

/**
 * Retourne le nombre de caractères de la String.
 */
size_t String::size() const {
   return strlen(string);
}

/**
 * Retourne un pointeur sur la chaine C.
 */
const char* String::getString() const {
   return (const char*)string;
}

/**
 * Retourne une référence vers le caractères à la position index.
 */
char& String::at(const unsigned int index) {
   if (index > (size() - 1) || size() == 0) {
      throw out_of_range("out of range");
   }
   return *(string + index);
}

/**
 * Retourne une copie du caractères à la position index.
 */
char String::at(const unsigned int index)const{
   String s(*this);
   char& c = s.at(index);
   return c;
}

/**
 * Teste l'égalite entre une String et une chaine C.
 */
bool String::equals(const char* s) const {
   return !strcmp(string, s);
}

/**
 * Teste l'égalite entre deux String.
 */
bool String::equals(const String& string) const {
   return equals(string.string);
}

/**
 * Remplace le contenu de la String par une chaine C.
 */
String& String::replace(const char* s) {
   if (string != s) { // Vérification s différent de string
      delete[] string; 
      init(strlen(s)); // Initialisation d'une nouvelle chaine de caractère
      strcpy(string, s);
   }
   return *this;
}

/**
 * Remplace le contenu de la String par une autre String.
 */
String& String::replace(const String& s) {
   return replace(s.string);
}

/**
 * Concatène une chaine C.
 */
String& String::append(const char* s){
   size_t taille = strlen(s)+ size();
   char* temp = new char[taille+1];
   temp[taille]='\0';
   strcpy(temp, string);         // copie la première partie
   strcpy(temp + size() , s);     // copie la seconde partie
   delete[] string;
   string = temp; 
   return *this;
}

/**
 * Concatène une String.
 */
String& String::append(const String& s) {
   return append(s.string);
}

/**
 * Retourne une sous-chaine de la position first à last compris.
 */
String String::substring(const unsigned int first, const unsigned int last) const {
   if (last > size())
      throw out_of_range("out of range");
   else if (first > last)
      throw range_error("range error");
   else if (first == last)
      return String(string[first]); // Retourne le caractère en position 
  
   int size = last-first+1;
   
   char* temp = new char[size + 1];
   temp[size]='\0';
   strncpy(temp,(string+first),size);
   
   String result = String(temp);
   delete[] temp;
   
   return result;
}

/**
 * Surcharges d'opérateurs
 */

/**
 * Ecriture sur un flux.
 */
ostream& operator<<(ostream& os, const String& s) {
   return os << s.string;
}

/**
 * Accès à un caractère.
 */
char& String::operator[](const unsigned int index) {
   return at(index);
}
char String::operator [](const unsigned int index) const{
   return at(index);
}


/**
 * Concaténation et affectation.
 */
String& String::operator+=(const String& s) {
   return append(s.string);
}

/**
 * Affectation.
 */
String& String::operator =(const String& s){
   if(string != s.string){
      replace(s);
   }
   return *this;
}

/**
 * Surcharges d'opérateurs par fonction
 */

/**
 * Utilisations des constructeurs, objet temporaire crée
 */
String operator+(const String& s1 , const String& s2){
   String cpy(s1);
   cpy+= s2;
   return cpy;
}
/**
 * Surcharge opérateur par une fonction
 */
bool operator ==(const String& s1, const String& s2) {
   return s1.equals(s2);
}

/**
 * Surcharge opérateur par une fonction
 */
bool operator !=(const String& s1, const String& s2) {
   return !(s1==s2);
}