/*
 * Déclaration de la class String qui permet la manipulation de chaines de
 * caractères. 
 *
 * Author : Mathieu Monteverde et Michela Zucca
 * 
 * Date  : 21.03.2017
 * File  : cString.h
 */

#ifndef CSTRING_H
#define	CSTRING_H
#include <iostream>
#include <cstdlib>
#include <stdexcept> // std::out_of_range

using namespace std;

/**
 * Classe String permettant de représenter des chaines de caractères. Cette classe 
 * offre différentes méthodes de manipulations de châines de caractères.
 */
class String {
    /**
     * Surcharge de l'opérateur d'écriture sur un flux.
     */
    friend ostream& operator<<(ostream& os, const String & s);
    
private:
   char* string; // Représentation interne de la chaîne
   
   /**
    * Initialisation d'une instance de la classe String. La méthode s'occupe
    * d'allouer les ressources nécessaires à la construction de l'objet.
    * @param len le nombre caractères dans la String à instancier (caractère de
    * terminaison non inclus).
    */
   void init(size_t len);

public:
    
   /**
    * Constructeur vide
    */
   String();
   
   /**
    * Constructeur à partir d'une chaine de caracètres C
    * @param s la chaine de caractère C à représenter
    */
   String(const char* s);
   
   /**
    * Constructeur à partir d'un caractère
    * @param c un caractère à convertir en String
    */
   String(const char c);
   
   /**
    * Constructeur à partir d'un entier
    * @param i l'entier à convertir en String
    */
   String(const int i);
   
   /**
    * Constructeur à partir d'un double
    * @param d le nombre réel à convertir en String
    */
   String(const double d);
   
   /**
    * Constructeur à partir d'un booleen
    * @param b la valeur booléenne à convertir en String
    */
   String(const bool b);
   
   /**
    * Constructeur de recopie
    * @param string la String à copier
    */
   String(const String & string);
   
   /**
    * Destructeur
    */
   ~String();
   
   /**
    * Détermine la taille de la chaine
    * @return le nombre de caractères contenus dans la châine de caractères
    */
   size_t size()const;
   
   /**
    * Permet d'obtenir une représentation de la String actuelle en const char*.
    * Le pointeur de retour pointe sur une chaine qui est valide tant que la 
    * String originale existe, qu'elle n'a pas été modifiée, et qu'aucun nouvel
    * appel à getString() n'a été fait. Ce comportmenet fonctionne par analogie  
    * à celui de la méthode c_str() de std::string. 
    * 
    * L'utilisateur ne devrait pas garder de copie de ce pointeur, car il pourrait
    * être invalidé au moment d'une utilisation future. Pour conserver une copie
    * de la chaine C en question, une utilisation de strcpy ou strncpy est 
    * par exemple conseillée.
    * @return un pointeur sur une représentation C de la String
    */
   const char* getString() const;
   
   /**
    * Retourne une référence vers le caractère à l'index donné
    * @param index position du caractère voulu
    * @return une référence vers caractère à l'index donné
    */   
   char& at(const unsigned int index);
   
   /**
    * Retourne une copie du caractère à l'index donné
    * @param index position du caractère voulu
    * @return une copie du caractère à l'index voulu
    */
   char at(const unsigned int index) const;
   
   /**
    * Teste si les deux chaine sont égales
    * @param s une châine C à comparer
    * @return true si s et l'instance String contiennent exactement la même 
    * séquence de caractères
    */
   bool equals(const char* s)const;
   
   /**
    * Teste si les deux String sont égales 
    * @param string la String à comparer
    * @return true si les deux châines sont identiques
    */
   bool equals(const String & string)const;
   
   /**
    * Remplace le contenu de l'instance String par le contenu de la chaine s
    * @param s une chaîne C
    */
   String& replace(const char* s);
   
   /**
    * Remplace la String par la String string
    * @param string une String
    */
   String& replace(const String & string);
   
   /**
    * Concatène une chaine C à la fin de la String actuelle
    * @param s une chaine C
    * @return une référence vers la chaine actuelle
    */
   String& append(const char* s);
   
   /**
    * Concatène une String à la fin de la String actuelle
    * @param string une String 
    * @return une référence vers la String actuelle
    */
   String& append(const String & string);
   
   /**
    * Retourne une sous-chaine String de la chaine
    * @param first l'index positions de début de la sous-chaine
    * @param last l'index position du  dernier caractère de la sous-chaine
    * @return une nouvelle sous-chaine String
    */
   String substring(const unsigned int first, const unsigned int last)const;

   /**
    * Surcharge de l'opérateur =. Fonctionne comme la méthode replace.
    * Utilisation implicite du constructeur sur le paramètre
    * @param s une String
    * @return une référence vers la String actuelle
    */
   String& operator=(const String & s);
   
   /**
    * Surcharge de l'opérateur +=. Fonctionne par analogie à la méthode append.
    * Utilisation implicite du constructeur sur le paramètre
    * @param s une String
    * @return une référence vers la String actuelle
    */
   String& operator+=(const String & s);
  
   /**
    * Surcharge de l'opérateur []. Permet d'obtenir une copie du caractère situé
    * à la position index. Fonctionne par analogie à la méthode at.
    * @param  la position du caractère
    * @return une copie du caractère
    */
   char operator[](const unsigned int index)const;
   
   /**
    * Surcharge de l'opérateur []. Permet d'obtenir une référence vers le 
    * caractère situé  à la position index. Fonctionne par analogie à la méthode at.
    * @param index la position du caractère
    * @return une référence vers le caractère
    */
   char& operator[](const unsigned int index);
};

/**
 * Surcharge de l'opérateur +. Fonctionne par analogie à la méthode append, mais
 * renvoie une nouvelle String issue de la concaténation
 * Utilisation implicite du constructeur sur les paramètres
 * @param s1
 * @param s2
 * @return 
 */
String operator+(const String& s1, const String& s2);

/**
 * Surcharge de l'opérateur ==. Fonctionne par analogie à la méthode equals.
 * Utilisation implicite du constructeur sur les paramètres
 * @param s1 première String à comparer
 * @param s2 Deuxième String à comparer
 * @return true si les String sont égales
 */
bool operator==(const String& s1, const String & s2);

/**
 * Surcharge de l'opérateur !=. 
 * Utilisation implicite du constructeur sur les paramètres
 * @param s1 première String à comparer
 * @param s2 deuxième String à comparer
 * @return true si les String ne sont pas égales
 */
bool operator!=(const String& s1, const String & s2);

#endif	/* CSTRING_H */

